color bg = color(12,35,64);
color hl = color(26,83,150);
color p1 = color(77,218,255);

int sqSize = 30;
int playerSize = 30;

int pX = 0;

void setup() {
  // Width and Height are defined by this function
  size(360, 480);
  background(bg);
}

void movePlayer() {
  pX = pX+sqSize;
}

void drawGrid() {
  int colCount = width / sqSize;
  int rowCount = height / sqSize;
  
  for (int i = 0; i < colCount; i = i+1) {
    for (int j = 0; j < rowCount; j = j+1) {
      stroke(hl);
      fill(hl, 0);
      rect(i * sqSize,j*sqSize,sqSize,sqSize);
    }
  }
}

void drawPlayer() {
  stroke(p1);
  fill(p1);
  rect(pX,0,playerSize,playerSize);
}

void draw() {
  drawGrid();
  drawPlayer();
  
  movePlayer();
}
